package com.biom4st3r.moenchantments;

import java.util.Random;
import java.util.Set;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;

/**
 * RenderHelper
 */
@Environment(EnvType.CLIENT)
public final class RenderHelper 
{

    private RenderHelper(){}


    // public static MeshBuilder createMesh()
    // {
    //     return getRenderer().meshBuilder();
    // }

    // public static Renderer getRenderer()
    // {
    //     return RendererAccess.INSTANCE.getRenderer();
    // }

    // public static MaterialFinder getMaterial()
    // {
    //     return getRenderer().materialFinder();
    // }

    public static int makeLight(int skylevel, int blocklevel)
    {
        return (skylevel << 24)+(blocklevel<< 4);
    }

    // public static boolean renderBlock(BlockRenderView view, BakedModel model, BlockState state, BlockPos pos, MatrixStack matrix, VertexConsumer vertexConsumer, boolean cull, Random random, long seed, int overlay)
    // {
    //     return ClientHelper.blockRenderManager.getModelRenderer().render(view, model, state, pos, matrix, vertexConsumer, cull, random, seed, overlay);
    // }
    // public static final Random random = new Random(4269420666L);
    // /**
    //  * Renders a block with manual Lighting
    //  * @param entry MatrixStack.peek()
    //  * @param consumer VertexConsumerProvider.getBuffer()
    //  * @param bakedModel whatever you want bro
    //  * @param r red
    //  * @param g green
    //  * @param b blue
    //  * @param overlay OverlayTexture.default
    //  * @param light 0x00S000B0 Sky Block
    //  */
    // public static void renderModel(MatrixStack.Entry entry,VertexConsumer consumer, BakedModel bakedModel,float r,float g, float b, int overlay,int light)
    // {
    //     ClientHelper.blockModelRenderer.render(entry, consumer, null, bakedModel, r, g, b, light, overlay);
    // }

    // public static void renderModel(MatrixStack.Entry entry,VertexConsumer consumer, BakedModel bakedModel,int color, int overlay,int light)
    // {
    //     float[] colors = intToRpg_F(color);
    //     ClientHelper.blockModelRenderer.render(entry, consumer, null, bakedModel, colors[0], colors[1], colors[2], light, overlay);
    // }

    // public static void renderModelDynamicLight(BlockRenderView view,BakedModel model,BlockState state,BlockPos pos, MatrixStack matrix,VertexConsumer vertexConsumer,boolean cull,int overlay)
    // {
    //     ClientHelper.blockModelRenderer.render(view, model, state, pos, matrix, vertexConsumer, cull, random, 4269420666L, overlay);
    // }
    
    /**
     * Renders lighting based on AO and the BlockPos in the BlockRenderView Provided;
     * @param model
     * @param state
     * @param pos
     * @param stack
     * @param vc
     * @param cull
     */
    // public static void renderModelDynamicLight(BakedModel model,BlockState state,BlockPos pos,MatrixStack stack,VertexConsumer vc,boolean cull)
    // {
    //     ClientHelper.blockModelRenderer.render(ClientHelper.world.get(), model, state, pos, stack, vc, cull, random, 4269420666L, OverlayTexture.DEFAULT_UV);
    // }
        public static Random random = new Random();
    /**
     * Assumes AO
     * @param model
     * @param state
     * @param pos
     * @param stack
     * @param vc
     * @param cull
     */
    // public static void renderModelAO(BakedModel model,BlockState state,BlockPos pos,MatrixStack stack,VertexConsumer vc, boolean cull)
    // {
    //     MinecraftClient.getInstance().getBlockRenderManager().renderSmooth(MinecraftClient.getInstance().player.world, model, state, pos, stack, vc, cull, random, 4269420666L, OverlayTexture.DEFAULT_UV);
    // }

    // public static void renderModelFlat(BakedModel model,BlockState state,BlockPos pos,MatrixStack stack,VertexConsumer vc, boolean cull)
    // {
    //     ClientHelper.blockModelRenderer.renderFlat(ClientHelper.world.get(), model, state, pos, stack, vc, cull, random, 4269420666L, OverlayTexture.DEFAULT_UV);
    // }

    // public static BakedModel getModel(BlockState state)
    // {
    //     return ClientHelper.bakedModelManager.getBlockModels().getModel(state);
    // }
    
    // public static BakedModel getModel(ModelIdentifier modelId)
    // {
    //     return ClientHelper.bakedModelManager.getModel(modelId);
    // }

    // public static BakedModel getModel(Block block)
    // {
    //     return getModel(block.getDefaultState());
    // }
    
    public static Sprite getBlockSprite(Identifier id)
    {
        return MinecraftClient.getInstance().getSpriteAtlas(SpriteAtlasTexture.BLOCK_ATLAS_TEX).apply(id);
    }

    // public static Sprite getSprite(Identifier atlasTexture, Identifier id)
    // {
    //     return ClientHelper.getTextureAtlas(atlasTexture).getSprite(id);
    // }

    // public static VertexConsumer getVertexConsumer(Identifier id,VertexConsumerProvider vertexConsumers, Function<Identifier, RenderLayer> layerFactory)
    // {
    //     return getBlockSprite(id).getTextureSpecificVertexConsumer(vertexConsumers.getBuffer(layerFactory.apply(id)));
    // }

    public static void renderBoxCulled(float x, float y, float z, float width, float depth, float height,int scale,Sprite sprite,
    VertexConsumer vc, MatrixStack matrix, int light, int color,Set<Direction> dirs)
    {
        
        if(dirs.contains(Direction.UP)) yFace(x,-y+(scale-height),-z+(scale-depth), width, depth, true, sprite, vc, matrix, light, color, scale); // up
        if(dirs.contains(Direction.SOUTH)) xFace(x,y,z, width, height, true, sprite, vc, matrix, light,color,scale); // south
        if(dirs.contains(Direction.NORTH)) xFace(-x+(scale-width),y,-z+(scale-depth), width, height, false, sprite, vc, matrix, light,color,scale); // north
        if(dirs.contains(Direction.EAST)) zFace(-x+(scale-width),y,-z+(scale-depth), depth, height, true, sprite, vc, matrix, light, color, scale); // east
        if(dirs.contains(Direction.WEST)) zFace(x,y,z, depth, height, false, sprite, vc, matrix, light, color, scale); // west
        if(dirs.contains(Direction.DOWN)) yFace(-x+(scale-width),y,-z+(scale-depth), width, depth, false, sprite, vc, matrix, light, color, scale); // down
    }

    // @Deprecated
    public static void renderBox(float x, float y, float z, float width, float depth, float height,int scale,Sprite sprite,
    VertexConsumer vc, MatrixStack matrix, int light, int color)
    {
        yFace(x,-y+(scale-height),-z+(scale-depth), width, depth, true, sprite, vc, matrix, light, color, scale); // up
        xFace(x,y,z, width, height, true, sprite, vc, matrix, light,color,scale); // south
        xFace(-x+(scale-width),y,-z+(scale-depth), width, height, false, sprite, vc, matrix, light,color,scale); // north
        zFace(-x+(scale-width),y,-z+(scale-depth), depth, height, true, sprite, vc, matrix, light, color, scale); // east
        zFace(x,y,z, depth, height, false, sprite, vc, matrix, light, color, scale); // west
        yFace(-x+(scale-width),y,-z+(scale-depth), width, depth, false, sprite, vc, matrix, light, color, scale); // down
    }

    public static void zFace(float x, float y, float z, float width, float height, boolean east, Sprite sprite,
    VertexConsumer vc, MatrixStack stack, int light, int color,float scale)
    {
        if(east)
        {
            x = scale-x;
        }
        else
        {
            width=-width;
            z = scale-z;
        }

        int[] normals = new int[]{
            1,1,1,
            1,1,1,
            1,1,1,
            1,1,1,};
        int ni = 0;
        float[] rgb = intToRpg_F(color);
        // float maxV = (sprite.getMaxV()-sprite.getMinV())*(height/scale);
        // maxV = sprite.getMinV()+maxV;   
        vertex(vc, stack, x/scale, y/scale,          z/scale,         sprite.getMinU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale, (y+height)/scale, z/scale,         sprite.getMinU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale, (y+height)/scale, (z+width)/scale, sprite.getMaxU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale, y/scale,          (z+width)/scale, sprite.getMaxU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);

    }

    public static void yFace(float x, float y, float z, float width, float height, boolean top, Sprite sprite,
        VertexConsumer vc, MatrixStack stack, int light, int color,float scale)
    {
        if(top)
        {
            y = scale-y;
        }
        else
        {
            width=-width;
            x=scale-x;
        }
        int[] normals = new int[]{
            1,1,1,
            1,1,1,
            1,1,1,
            1,1,1,};
        int ni = 0;
        float[] rgb = intToRpg_F(color);
        vertex(vc, stack, x/scale,         y/scale, z/scale,        sprite.getMinU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale,         y/scale, (z+height)/scale, sprite.getMinU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, (x+width)/scale, y/scale, (z+height)/scale, sprite.getMaxU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, (x+width)/scale, y/scale, z/scale,          sprite.getMaxU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);

    }

    public static void xFace(float x, float y, float z, float width, float height, boolean south, Sprite sprite,
        VertexConsumer vc, MatrixStack stack, int light,int color, float scale)
    {
        int[] normals = new int[]{
            1,1,1,
            1,1,1,
            1,1,1,
            1,1,1,};
        int ni = 0;
        if(south)
        {
            z = scale-z;
        }
        else
        {
            width=-width;
            x = scale-x;
        }
        float[] rgb = intToRpg_F(color);
        // For water levels
        // float maxV = (sprite.getMaxV()-sprite.getMinV())*(height/scale);
        // maxV = sprite.getMinV()+maxV;
        vertex(vc, stack, x/scale,          y/scale,            z/scale, sprite.getMinU(),sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);// bottomleft
        vertex(vc, stack, (x+width)/scale,  y/scale,            z/scale, sprite.getMaxU(),sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);//top left
        vertex(vc, stack, (x+width)/scale,  (y+height)/scale,   z/scale, sprite.getMaxU(),sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);// top right
        vertex(vc, stack, x/scale,          (y+height)/scale,   z/scale, sprite.getMinU(),sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);//bottom right
    }

    // public static void zFace(float x, float y, float z, float width, float height, boolean east, Sprite sprite,
    //         VertexConsumer vc, MatrixStack stack, int light)
    // {
    //     zFace(x, y, z, width, height, east, sprite, vc, stack, light, 0xFFFFFFFF,16);
    // }

    // public static void yFace(float x, float y, float z, float width, float height, boolean top, Sprite sprite,
    //         VertexConsumer vc, MatrixStack stack, int light)
    // {
    //     yFace(x, y, z, width, height, top, sprite, vc, stack, light, 0xFFFFFFFF,16);
    // }

    // public static void xFace(float x, float y, float z, float width, float height, boolean south, Sprite sprite,
    //         VertexConsumer vc, MatrixStack stack, int light)
    // {
    //     xFace(x, y, z, width, height, south, sprite, vc, stack, light, 0xFFFFFFFF,16);
    // }

    public static void vertex(VertexConsumer vc, MatrixStack stack,float x,float y,float z,float u, float v,float r,float g,float b,float a,int overlayuv,int lightuv,float nx,float ny,float nz)
    {
        vc.vertex(stack.peek().getModel(),x,y,z)
            .color(r,g,b,a)
            .texture(u, v)
            .overlay(OverlayTexture.DEFAULT_UV)
            .light(lightuv)
            .normal(stack.peek().getNormal(),nx,ny,nz)
            .next();
    }

    public static void vertex(VertexConsumer vc, MatrixStack stack,float x,float y,float z,float u,float v,int overlayuv,int lightuv,float nx,float ny,float nz)
    {
        vertex(vc, stack, x, y, z,u,v, 1.0f, 1.0f, 1.0f, 1.0f, overlayuv, lightuv, nx, ny, nz);
    }
 
    public static void vertex(VertexConsumer vc, MatrixStack stack,float x,float y,float z,float u,float v,int lightuv,float nx,float ny,float nz)
    {
        vertex(vc, stack, x, y, z,u,v, OverlayTexture.DEFAULT_UV, lightuv, nx, ny, nz);
    }

    public static int[] intToRgb_I(int i)
    {
        return new int[]{(i >> 0 & 0xFF),(i >> 8 & 0xFF),(i >> 16 & 0xFF)};
    }
    
    public static float[] intToRpg_F(int i)
    {
        return new float[]{(((i >> 16 & 0xFF))/256.0F),(((i >> 8 & 0xFF))/256.0F),(((i >> 0 & 0xFF))/256.0F)};
    }

    
}