package com.biom4st3r.moenchantments;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;

import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.util.Util;

/**
 * Plugin
 */
public class Plugin implements IMixinConfigPlugin {
    BioLogger logger = ModInit.logger;

    

    @Override
    public void onLoad(String mixinPackage) {
        // ScriptEngineManager manager = new ScriptEngineManager();
        // ScriptEngine engine = manager.getEngineByName("javascript");
        // engine.setContext(ScriptContext);

    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }
    public static Map<String,Boolean> disabler = Util.make(Maps.newHashMap(), (map)->
    {
        map.put("com.biom4st3r.moenchantments.mixin.soulbound/PlayerInventoryMxn", ModInit.config.EnableSoulBound);
        map.put("com.biom4st3r.moenchantments.mixin.soulbound/ServerPECopyFromMxn", ModInit.config.EnableSoulBound);
        map.put("com.biom4st3r.moenchantments.mixin.bowaccuracy/ProjectileEntityMxn", FabricLoader.getInstance().isModLoaded("extrabows"));
        map.put("com.biom4st3r.moenchantments.mixin.FillFix",FabricLoader.getInstance().isDevelopmentEnvironment());
        map.put("com.biom4st3r.moenchantments.mixin.SkipEulaMxn",FabricLoader.getInstance().isDevelopmentEnvironment());
        map.put("com.biom4st3r.moenchantments.mixin.SpeedDisplay", false);
    });

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        if(disabler.containsKey(mixinClassName))
        {
            ModInit.logger.debug("%s: %s", mixinClassName,disabler.get(mixinClassName));
            
            return disabler.get(mixinClassName);
        }
        
        ModInit.logger.debug("Applying %s", mixinClassName);
        return true;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    
}