package com.biom4st3r.moenchantments;

import java.util.Set;

import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.api.events.LivingEntityDamageEvent;
import com.biom4st3r.moenchantments.api.events.OnBlockBreakAttemptEvent;
import com.biom4st3r.moenchantments.api.events.OnBowArrowCreationEvent;
import com.biom4st3r.moenchantments.api.events.OnCrossBowArrowCreationEvent;
import com.biom4st3r.moenchantments.interfaces.ProjectileEntityEnchantment;
import com.biom4st3r.moenchantments.logic.ChaosArrowLogic;
import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.logic.VeinMinerLogic;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity.PickupPermission;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

public final class EventHandlers {
    private EventHandlers(){}

    private static void addProjectileEnchant(PersistentProjectileEntity arrow,LivingEntity shooter,ItemStack bowItem)
    {
        ((ProjectileEntityEnchantment)arrow).setEnchantmentLevel(EnchantmentRegistry.BOW_ACCURACY,EnchantmentRegistry.BOW_ACCURACY.getLevel(bowItem));
        ((ProjectileEntityEnchantment)arrow).setEnchantmentLevel(EnchantmentRegistry.BOW_ACCURACY_CURSE,EnchantmentRegistry.BOW_ACCURACY_CURSE.getLevel(bowItem));
        ((ProjectileEntityEnchantment)arrow).setEnchantmentLevel(EnchantmentRegistry.GRAPNEL,EnchantmentRegistry.GRAPNEL.getLevel(bowItem));
        if(ModInit.config.EnableArrowChaos && ChaosArrowLogic.makePotionArrow(shooter, arrow, shooter.getRandom()))
        {
            arrow.pickupType = PickupPermission.CREATIVE_ONLY;
        }
    }
    private static Set<EntityType<?>> biom4st3r_VALID = Sets.<EntityType<?>>newHashSet(EntityType.ARROW,EntityType.SPECTRAL_ARROW);
    public static void init()
    {
        LivingEntityDamageEvent.EVENT.register((damageSource,damage,entity,ci)->
        {
            int enderProLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.ENDERPROTECTION, (LivingEntity)entity);
            if (enderProLvl > 0) {
                if (EnderProtectionLogic.doLogic(damageSource, enderProLvl, (LivingEntity)entity)) { 
                    ci.setReturnValue(false);
                }
            }
        });
        LivingEntityDamageEvent.EVENT.register((damageSource,damage,entity,ci)->
        {
            if ((entity) instanceof TameableEntity && damageSource.getAttacker() instanceof PlayerEntity) 
            {
                PlayerEntity attacker = (PlayerEntity) damageSource.getAttacker();
                TameableEntity defender = (TameableEntity) entity;
                if (EnchantmentSkeleton.hasEnchant(EnchantmentRegistry.TAMEDPROTECTION, attacker.getMainHandStack())
                        && defender.getOwnerUuid() != ModInit.uuidZero) 
                        {
                    if (ModInit.config.TameProtectsOnlyYourAnimals) 
                    {
                        if (defender.isOwner(attacker)) {
                            ci.setReturnValue(false);
                        }
                    } 
                    else 
                    {
                        ci.setReturnValue(false);
                    }
                }
            }
        });
        OnBlockBreakAttemptEvent.EVENT.register((manager,pos,reason)->
        {
            if(reason.isSuccessfulAndEffective() && !manager.player.isCreative())
            {
                PlayerEntity pe = manager.player;
                World world = pe.getEntityWorld();
                VeinMinerLogic.tryVeinMining(world, pos, world.getBlockState(pos), pe);
            }
            return ActionResult.PASS;
        });
        OnCrossBowArrowCreationEvent.EVENT.register((arrow,crossbow,ArrowItem,shooter)->
        {
            addProjectileEnchant(arrow, shooter, crossbow);
        });
        OnBowArrowCreationEvent.EVENT.register((bow,arrowStack,arrowEntity,player,elapsed,pullProg)->
        {
            addProjectileEnchant(arrowEntity, player,bow);
        });
        LivingEntityDamageEvent.EVENT.register((source,damage,entity,ci)->
        {
            if(source.isProjectile() && biom4st3r_VALID.contains(source.getSource().getType()))
            {
                if(((ProjectileEntityEnchantment)source.getSource()).getEnchantmentLevel(EnchantmentRegistry.GRAPNEL) > 0)
                {
                    damage=0.5F;
                }
            }
        });
        // Item
        // ClientPlayNetworkHandler
    }

    // public static void hoarding() {
    //     LivingEntityDamageEvent.EVENT.register((damageSource,damage,entity,ci)->
    //     {
    //         if(damageSource.getAttacker() != null)
    //         {
    //             LivingEntity attacker = (LivingEntity) damageSource.getAttacker();
    //             int hoardingLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.HOARDING, attacker);
    //             if(hoardingLvl > 0 && !(entity instanceof PlayerEntity) && entity instanceof Monster)
    //             {
    //                 Random random = entity.getEntityWorld().getRandom();
    //                 World world = entity.getEntityWorld();
    //                 for(int i = 0; i < 10; i++);
    //                 {
    //                     int x = entity.getBlockPos().getX() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
    //                     int y = entity.getBlockPos().getY() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
    //                     int z = entity.getBlockPos().getZ() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
    //                     BlockPos spawnPos = new BlockPos(x, y, z);
    //                     if(checkBox(world, new Box(spawnPos,spawnPos.up(3)), (blockstate)-> blockstate.isAir()))
    //                     {
    //                         world.spawnEntity(entity.getType().create(world));
    //                     }
    //                 }
                    
    //             }
    //         }
    //     });
    // }
    public static void familiarity() {

    }
    public static void enderProtection() {

    }
    public static void veinMining() {

    }
    public static boolean checkBox(World world, Box box, Predicate<BlockState> condition)
    {
        boolean result = true;
        for(int x = (int) box.minX; x < box.maxX; x++)
        {
            for(int y = (int) box.minY; y < box.maxY; y++)
            {
                for(int z = (int) box.minZ; z < box.maxZ; z++)
                {
                    result &= condition.apply(world.getBlockState(new BlockPos(x, y, z)));
                }
            }
        }
        return result;
    }

}