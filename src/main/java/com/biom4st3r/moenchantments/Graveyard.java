package com.biom4st3r.moenchantments;

import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.api.MoEnchantBuilder;

import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Items;

public class Graveyard {
    public static final EnchantmentSkeleton BUILDERS_WAND = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.HAND_SLOTS)
    .treasure(true)
    .minpower((level)->{return 10;})
    .enabled(false)
    .isAcceptible((itemstack)->{return itemstack.getItem() == Items.STICK;})
    .build("builders_wand");
    //https://www.reddit.com/r/minecraftsuggestions/comments/dx8zom/shield_enchantments/
    public static final EnchantmentSkeleton SHIELD_REFLECT = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, EquipmentSlot.OFFHAND)
    .maxlevel(3)
    .treasure(true)
    .enabled(false)
    .build("reflectarrow");
public static final EnchantmentSkeleton TRAINING_WEAPON = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
    .maxlevel(1)
    .treasure(true)
    .enabled(false)//ModInit.config.EnableTrainingWeapon)
    .build("training_weapon"); // Provide more xp from kill. but reduce damage;
public static final EnchantmentSkeleton VAMPIRISM = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
    .treasure(true)
    .minpower((level)-> {return 10;})
    .maxpower((level)->{return 15;})
    .enabled(false)//ModInit.config.EnableVampirism)
    .build("vampirism");
public static final EnchantmentSkeleton HOARDING = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
    .curse(true)
    .minpower((level)->{return 20;})
    .maxpower((level)->{return 30;})
    .enabled(false)//ModInit.config.EnableHording)
    .build("hording");
public static final EnchantmentSkeleton SHIELD_BASH = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.HAND_SLOTS)
    .treasure(true)
    .minpower((level)->{return 10;})
    .enabled(false)
    .addExclusive(SHIELD_REFLECT)
    .build("shield_bash");
public static final EnchantmentSkeleton TELEPORTING_ARROW = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
    .treasure(true)
    .minpower((l)->30)
    .enabled(false)
    .addExclusive(Enchantments.MULTISHOT).addExclusive(Enchantments.INFINITY).addExclusive(EnchantmentRegistry.ARROW_CHAOS)
    .build("teleport_arrow");
public static final EnchantmentSkeleton ARROW_DISTANCE = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
    .minpower((level)->8)
    .enabled(false)
    .build("oomph");
}