
package com.biom4st3r.moenchantments.mixin;

import java.util.Iterator;
import java.util.List;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mixin(EnchantmentHelper.class)
public abstract class EnchantmentHelperMixin {
/*

slice = @Slice(
   from = @At(value="INVOKE",target="java/util/Iterator.next()Ljava/lang/Object;"),
   to = @At(value="INVOKE",target="net/minecraft/enchantment/Enchantment.isTreasure()Z")
   ),
*/
   @Inject(
      at = @At(
         value="INVOKE",
         target="net/minecraft/enchantment/Enchantment.isTreasure()Z",
         ordinal = 0,
         shift = Shift.BEFORE
         ),
      method = "getPossibleEntries",
      cancellable = false,
      locals = LocalCapture.CAPTURE_FAILHARD)
   private static void isAcceptibleForMoEnchantment(int power, ItemStack stack, 
      boolean treasureAllowed, CallbackInfoReturnable<List<EnchantmentLevelEntry>> cir, 
      List<EnchantmentLevelEntry> list, Item item, boolean bl, Iterator<Enchantment> var6, 
      Enchantment enchantment)
   {
      // EnchantCommand
      while(enchantment instanceof EnchantmentSkeleton && var6.hasNext())
      {
         EnchantmentSkeleton mEnch = (EnchantmentSkeleton) enchantment;
         if(!mEnch.isAcceptableItem(stack) || (enchantment.isTreasure() && !treasureAllowed) || !enchantment.isAvailableForRandomSelection())
         {
            enchantment = var6.next();
         }
         else
         {
            for (int i = enchantment.getMaxLevel(); i > enchantment.getMinLevel() - 1; --i) {
               if (power >= enchantment.getMinPower(i) && power <= enchantment.getMaxPower(i)) {
                  list.add(new EnchantmentLevelEntry(enchantment, i));
                  break;
               }
            }
            var6.next();
         }
      }
   }

   @Inject(
      at = @At(
         value="INVOKE",
         target="java/util/List.add(Ljava/lang/Object;)Z",
         ordinal = -1,
         shift = Shift.BEFORE),
      method = "getPossibleEntries",
      cancellable = false,
      locals = LocalCapture.CAPTURE_FAILHARD)
   private static void invertEndermanCurse(int power, ItemStack stack, 
      boolean treasureAllowed, CallbackInfoReturnable<List<EnchantmentLevelEntry>> cir, 
      List<EnchantmentLevelEntry> list, Item item, boolean bl, Iterator<Enchantment> var6, 
      Enchantment enchantment, int i)
   {
      if(enchantment == EnchantmentRegistry.ENDERPROTECTION)
      {
         i = (i == 3 ? 1 : i == 1 ? 3 : i);
      }
   }
}
