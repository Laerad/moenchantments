package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.moenchantments.interfaces.ProjectileEntityEnchantment;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

@Mixin(ProjectileEntity.class)
public abstract class ProjectileEntityEnchantmentImpl implements ProjectileEntityEnchantment {
    
    private Object2IntMap<Enchantment> enchantments = new Object2IntOpenHashMap<>();

    private void biom4st3r_initMap()
    {
        // PersistentProjectileEntity
        enchantments = new Object2IntOpenHashMap<>();
    }

    @Inject(at = @At(value = "RETURN"), method = "<init>*", cancellable = false, locals = LocalCapture.NO_CAPTURE)
    public void ctorInject(CallbackInfo ci) {
        biom4st3r_initMap();
    }
    
    @Override
    public int getEnchantmentLevel(Enchantment e) {
        return enchantments.getOrDefault(e, 0);
    }

    @Override
    public void setEnchantmentLevel(Enchantment e, int level) {
        if(level > 0) 
        {
            enchantments.put(e, level);
        }
    }

    @Inject(
        at = @At(value = "TAIL"),
        method = "readCustomDataFromTag",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    public void biom4st3r_readCustomData(CompoundTag tag,CallbackInfo ci)
    {
        biom4st3r_initMap();
        ListTag lt = (ListTag) tag.get("biom4st3r_enchantments");
        lt.forEach((t)->
        {
            CompoundTag ct = (CompoundTag) t;
            enchantments.put(Registry.ENCHANTMENT.get(new Identifier(ct.getString("e"))), ct.getInt("i"));
        });
    }

    @Inject(
        at = @At(value = "TAIL"),
        method = "writeCustomDataToTag",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    
    public void biom4st3r_writeCustomData(CompoundTag tag,CallbackInfo ci)
    {
        ListTag lt = new ListTag();
        enchantments.forEach((e,i)->
        {
            CompoundTag ct = new CompoundTag();
            ct.putString("e", Registry.ENCHANTMENT.getId(e).toString());
            ct.putInt("l", i);
            lt.add(ct);
        });
        tag.put("biom4st3r_enchantments", lt);
    }
    
}