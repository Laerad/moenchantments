package com.biom4st3r.moenchantments.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.server.Main;
import net.minecraft.server.dedicated.EulaReader;

/**
 * DevSkipEulaMixin
 */
@Mixin(Main.class)
public abstract class SkipEulaMxn {

    @Redirect(
        method = "main",
        at = @At(value = "INVOKE", 
        target = "net/minecraft/server/dedicated/EulaReader.isEulaAgreedTo()Z",
        ordinal = 0)
    )
    private static boolean eulaisAgreedTo(EulaReader reader) //why are you static?
    {
        return true;
    }
    
    
}