package com.biom4st3r.moenchantments.mixin.events;

import java.util.concurrent.TimeUnit;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.api.events.OnCrossBowArrowCreationEvent;
import com.google.common.base.Stopwatch;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(CrossbowItem.class)
public abstract class OnCrossBowArrowCreationEventMxn {

    private static Stopwatch sw = Stopwatch.createUnstarted();
    @Inject(
        at = @At(value = "RETURN"),
        method = "createArrow(Lnet/minecraft/world/World;Lnet/minecraft/entity/LivingEntity;Lnet/minecraft/item/ItemStack;Lnet/minecraft/item/ItemStack;)Lnet/minecraft/entity/projectile/PersistentProjectileEntity;",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    private static void biom4st3r_onCrossbowArrowCreation(World world, LivingEntity entity, ItemStack crossbow, ItemStack arrow,CallbackInfoReturnable<PersistentProjectileEntity> ci)
    {
        sw.start();
        OnCrossBowArrowCreationEvent.EVENT.invoker().onCreation(ci.getReturnValue(), crossbow, arrow, entity);
        ModInit.logger.debug("OnCrossBowArrowCreationEvent " + sw.stop().elapsed(TimeUnit.MICROSECONDS));
        sw.reset();
    }
}