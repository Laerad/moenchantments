package com.biom4st3r.moenchantments;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.biom4st3r.moenchantments.networking.Packets;
import com.google.common.collect.Sets;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.entry.AlternativeEntry;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.entry.LootPoolEntryTypes;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModInit implements ModInitializer {
	public static final String MODID = "biom4st3rmoenchantments";
	public static MoEnchantsConfig config;
	public static final BioLogger logger = new BioLogger("MoEnchantments");
	public static final UUID uuidZero = new UUID(0L, 0L);
	public static IntArrayList int_block_whitelist = new IntArrayList(100);
	public static List<Item> autoSmelt_blacklist = new ArrayList<Item>();
	public static boolean lockAutoSmeltSound = false;
	public static Set<Block> blocks_without_loot_function = Sets.newHashSet();
	public static boolean extraBowsFound = false;

	static
	{
		config = MoEnchantsConfig.init(config);
		// HashMap
	}

	@Override
	public void onInitialize() {
		extraBowsFound = FabricLoader.getInstance().isModLoaded("extrabows");
		EnchantmentRegistry.init();
		
		EventHandlers.init();
		Packets.init();
		// Please show me the light of a better solution. I need to know if Fortune will apply when the a block is broken
		LootTableLoadingCallback.EVENT.register((resourceManager, manager, id, supplier, setter)->
		{
			if(!config.ApplyLootingToAlphaFire) return;
			String[] path = id.getPath().split("/");
			
			for(String x : config.veinMinerBlockWhiteList)
			{
				if((path.length == 2 ? path[1] : path[0]).contains(x.split(":")[1]))
				{
					LootTable lt =  manager.getTable(id);
					label0:for(LootPool pool : lt.pools)
					{
						// Oh God
						for(LootPoolEntry entry : pool.entries)
						{
							if(entry.getType() == LootPoolEntryTypes.ALTERNATIVES)
							{ //Please
								for(LootPoolEntry lpe : ((AlternativeEntry)entry).children)
								{
									for(LootFunction function : ((LeafEntry)lpe).functions)
									{ // Make it stop
										if(function instanceof ApplyBonusLootFunction && ((ApplyBonusLootFunction)function).enchantment == Enchantments.FORTUNE)
										{
											Registry.BLOCK.getOrEmpty(new Identifier(x)).ifPresent((b)->blocks_without_loot_function.remove(b));
											break label0;
										}
									}
								}
							}
						}
					}
				}
			}
		});
	}


	public static void whitelistToBlock() {
		for (String s : config.veinMinerBlockWhiteList) {
			Block block = Registry.BLOCK.get(new Identifier(s));
			if (block != Blocks.AIR) 
			{
				blocks_without_loot_function.add(block);
				int_block_whitelist.add(Registry.BLOCK.getRawId(block));
				logger.log("added %s as %s", block.getLootTableId().toString(),Registry.BLOCK.getRawId(block));
				continue;
			}
			else
			{
				logger.log("%s was not found", s);
			}
		}
	}

	public static void blacklistAutosmelt()
	{
		for(String s : config.AutoSmeltBlackList)
		{
			Item i = Registry.ITEM.get(new Identifier(s));
			if(i != Items.AIR)
			{
				autoSmelt_blacklist.add(i);
				logger.log("added %s to autosmelt blacklist",s);
			}
			else
			{
				logger.log("%s was not found", s);
			}
		}
	}

}